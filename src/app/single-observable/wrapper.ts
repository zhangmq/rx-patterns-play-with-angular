import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  template: `
    <input [(ngModel)]="value" placeholder="input for trigger fetch" />  
    <app-single-observable [searchQuery]="value"><app-single-observable>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SingleObservableWrapperComponent {
  value = '';
}