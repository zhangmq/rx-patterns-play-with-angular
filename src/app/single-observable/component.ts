import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { BehaviorSubject, of, Subject, merge } from 'rxjs';
import { switchMap, delay, catchError, map, startWith, scan, tap, withLatestFrom, debounceTime } from 'rxjs/operators';

interface WithLoadingState<T> {
  data: T,
  error: any;
  loading: false;
}

@Component({
  selector: 'app-single-observable',
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SingleObservableComponent {
  // mock a search query, when use multi input, all query input should merged to one obervable
  // searchQuery$ = combinLatest(this.searchQuery1$, this.searchQuery2$, (query1, query2) => ({ query1, query2 }))
  @Input()
  get searchQuery() {
    return this.searchQuery$.value;
  }
  set searchQuery(value: string) {
    this.searchQuery$.next(value);
  }
  private searchQuery$ = new BehaviorSubject<string>('');

  use_input_search_query$ = this.searchQuery$.pipe(
    switchMap(searchQuery => this.fetchWithSearchQuery(searchQuery).pipe(
      catchError(() => of(null)),
    )),
  );

  use_input_search_query_and_with_loading_state$ = this.searchQuery$.pipe(
    switchMap(searchQuery => this.fetchWithSearchQuery(searchQuery).pipe(
      map(data => ({ data, error: null, loading: false })),
      catchError(error => of({ data: null, error, loading: false })),
      startWith({ data: null, error: null, loading: true }),
    )),
  );

  use_input_search_query_and_with_loading_state_and_keep_latest_success_state_when_loading$ = this.searchQuery$.pipe(
    switchMap(searchQuery => this.fetchWithSearchQuery(searchQuery).pipe(
      map(data => (_: WithLoadingState<any>) => ({ error: null, data, loading: false })),
      catchError(error => of(() => ({ data: null, error, loading: false }))),
      startWith((prevState: WithLoadingState<any>) => ({ ...prevState, error: null, loading: true })),
    )),
    scan((prevState: WithLoadingState<any>, action: (prevState: WithLoadingState<any>) => WithLoadingState<any>) => action(prevState)),
    startWith({ data: null, error: null, loading: false }),
  );

  //only counting pulling timeout when prev fetch completed
  fetchComplete$ = new Subject<void>();
  use_input_search_query_and_pulling$ = merge(
    this.searchQuery$,
    this.fetchComplete$.pipe(
      debounceTime(5 * 1000),
      withLatestFrom(this.searchQuery$, (_, searchQuery) => searchQuery),
    ),
  ).pipe(
    switchMap(searchQuery => this.fetchWithSearchQuery(searchQuery).pipe(
      map(data => ({ data, error: null, loading: false })),
      catchError(error => of({ data: null, error, loading: false })),
      tap(() => {
        this.fetchComplete$.next();
      }),
      startWith({ data: null, error: null, loading: true }),
    )),
  );

  constructor() { }

  fetchWithSearchQuery(searchQuery: string) {
    return of(new Array(3).fill(searchQuery)).pipe(delay(1000));
  }

}
