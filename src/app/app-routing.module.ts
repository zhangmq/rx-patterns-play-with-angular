import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SingleObservableWrapperComponent } from './single-observable/wrapper';

const routes: Routes = [
  { path: '', redirectTo: '/single-observable', pathMatch: 'full' },
  {
    path: 'single-observable',
    component: SingleObservableWrapperComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
