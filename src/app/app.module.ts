import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SingleObservableComponent } from './single-observable/component';
import { SingleObservableWrapperComponent } from './single-observable/wrapper';

@NgModule({
  declarations: [
    AppComponent,
    SingleObservableComponent,
    SingleObservableWrapperComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
